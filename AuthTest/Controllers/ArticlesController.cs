﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AuthTest.Data;
using AuthTest.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace AuthTest.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public ArticlesController(ApplicationDbContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Articles
        public async Task<IActionResult> Index()
        {
            const int summaryLength = 250;
            var articles = await _context.Articles.ToListAsync();

            foreach (var article in articles)
            {
                var shortContent = new List<string>();
                var words = article.Content.Split(' ');
                var letterCount = 0;

                foreach (var word in words)
                {
                    if (letterCount < summaryLength)
                    {
                        var currentWord = word;
                        letterCount += word.Length + 1;
                        if(letterCount >= summaryLength)
                        {
                            currentWord += "...";
                        }
                        shortContent.Add(currentWord);
                    }
                }

                article.Content = string.Join(' ', shortContent.ToArray());
            }

            return View(articles.OrderBy(s => s.CreationDate));
        }

        // GET: Articles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appArticle = await _context.Articles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appArticle == null)
            {
                return NotFound();
            }

            return View(appArticle);
        }

        // GET: Articles/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Articles/Create
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Content")] AppArticle appArticle)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            appArticle.AuthorId = user.Id;
            appArticle.CreationDate = DateTime.Now;
            appArticle.LastUpdatedDate = DateTime.Now;

            //fix this
            appArticle.AuthorName = user.Email;

            if (ModelState.IsValid)
            {
                _context.Add(appArticle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(appArticle);
        }

        // GET: Articles/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appArticle = await _context.Articles.FindAsync(id);
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var roles = await _userManager.GetRolesAsync(user);

            if (appArticle == null)
            {
                return NotFound();
            }

            if (user.Id != appArticle.AuthorId && !roles.Contains("Admin"))
            {
                return NotFound();
            }

            return View(appArticle);
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AuthorId,CreationDate,Title,Content,AuthorName")] AppArticle appArticle)
        {
            if (id != appArticle.Id)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);
            var roles = await _userManager.GetRolesAsync(user);
            if(user.Id != appArticle.AuthorId && !roles.Contains("Admin"))
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    appArticle.LastUpdatedDate = DateTime.Now;
                    _context.Update(appArticle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppArticleExists(appArticle.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(appArticle);
        }

        // GET: Articles/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appArticle = await _context.Articles
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appArticle == null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(HttpContext.User);
            var roles = await _userManager.GetRolesAsync(user);
            if (user.Id != appArticle.AuthorId && !roles.Contains("Admin"))
            {
                return NotFound();
            }

            return View(appArticle);
        }

        // POST: Articles/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appArticle = await _context.Articles.FindAsync(id);
            
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var roles = await _userManager.GetRolesAsync(user);
            if (user.Id != appArticle.AuthorId && !roles.Contains("Admin"))
            {
                return NotFound();
            }

            _context.Articles.Remove(appArticle);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AppArticleExists(int id)
        {
            return _context.Articles.Any(e => e.Id == id);
        }
    }
}

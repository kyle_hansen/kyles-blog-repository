﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AuthTest.Models
{
    public class AppArticle
    {
        [Key]
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public DateTime LastUpdatedDate { get; set; } = DateTime.Now;
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string AuthorName { get; set; }
    }
}

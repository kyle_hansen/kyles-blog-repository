﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthTest.Data.Migrations
{
    public partial class AddedRichTextToArticle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RichText",
                table: "Articles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RichText",
                table: "Articles");
        }
    }
}
